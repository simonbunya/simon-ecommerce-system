namespace Shops.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ShopID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ShopID", c => c.String());
            AddColumn("dbo.AspNetUsers", "CountryID", c => c.String());
            AddColumn("dbo.AspNetUsers", "Person_ID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Person_ID");
            DropColumn("dbo.AspNetUsers", "CountryID");
            DropColumn("dbo.AspNetUsers", "ShopID");
        }
    }
}
