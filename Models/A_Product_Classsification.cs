//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Shops.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class A_Product_Classsification
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public A_Product_Classsification()
        {
            this.A_Product = new HashSet<A_Product>();
        }
    
        public int Commodity_Class_Code { get; set; }
        public string Commodity_Class_Desc { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<A_Product> A_Product { get; set; }
    }
}
